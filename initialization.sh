#!/bin/bash
# This script will install ParaView and the VTK library
# in a folder nammed ParaView in the same directory that this script

echo "===================="
echo "Downloading Paraview"
echo "===================="
curl -O "http://www.paraview.org/paraview-downloads/download.php?submit=Download&version=v5.0&type=binary&os=linux64&downloadFile=ParaView-5.0.0-Qt4-OpenGL2-MPI-Linux-64bit.tar.gz"
mv download.php?submit=Download&version=v5.0&type=binary&os=linux64&downloadFile=ParaView-5.0.0-Qt4-OpenGL2-MPI-Linux-64bit.tar.gz ParaView-5.0.0-Qt4-OpenGL2-MPI-Linux-64bit.tar.gz

echo "==================="
echo "Extracting Paraview"
echo "==================="
tar xvf ParaView-5.0.0-Qt4-OpenGL2-MPI-Linux-64bit.tar.gz
mv ParaView-5.0.0-Qt4-OpenGL2-MPI-Linux-64bit ParaView
cd ParaView
paraview_directory=$(pwd)

echo "==============="
echo "Downloading VTK"
echo "==============="
curl -O http://www.vtk.org/files/release/7.0/VTK-7.0.0.tar.gz
tar xvf VTK-7.0.0.tar.gz

echo "============"
echo "Building VTK"
echo "============"
cd VTK-7.0.0
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$paraview_directory -DCMAKE_CXX_FLAGS="-march=native -mfpmath=sse" ..
make -j3
make install

echo "==================="
echo "Downloading my code"
echo "==================="
cd ../..
git clone https://bitbucket.org/Goligo/voxelsplotter.git
cd voxelsplotter

echo "==============="
echo "Buiding my code"
echo "==============="
mkdir build
cd build
vtk_dir="$paraview_directory/lib/cmake/vtk-7.0"
cmake -DVTK_DIR=$vtk_dir ..
make -j3
echo "==============="
echo "Testing my code"
echo "==============="
./VoxelsPlotter ../data/wrenchworkspace_trajectory.txt
