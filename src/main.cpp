#include <iostream>
#include <fstream>
#include <string>
#include "VoxelsPlotter.h"

int readFile(std::string fileName, VoxelsPlotter& vp)
{
  ifstream dataFile(fileName, ios::in);
  if (dataFile) {
    double xMin, xMax, yMin, yMax, zMin, zMax, dump;
    int color;
    while (dataFile >> xMin >> xMax >> yMin >> yMax >> zMin >> zMax\
        >> dump >> dump >> dump >> dump >> dump >> dump >> color) {
      //vp.addVoxel(xMin, xMax, yMin, yMax, zMin, zMax, color);
      // Force zMax to 1.0
      vp.addVoxel(xMin, xMax, yMin, yMax, zMin, 0.01, color);
    }
    dataFile.close();
    return EXIT_SUCCESS;
  } else {
    std::cerr << "Failed to open the file \"" << fileName << "\"" << std::endl;
    return EXIT_FAILURE;
  }
}

int main(int argc, char** argv)
{
  // test if the filename was given
  if (argc > 1) {
    std::string fileName(argv[1]);
    std::cout << "Reading the file: \"" << fileName << "\"" << std::endl;

    VoxelsPlotter vp;
    if (readFile(fileName, vp) == EXIT_SUCCESS) {
      vp.plot();
      vp.writeFile("out");
    }
  } else {
    std::cout << "Usage:\n\t" << argv[0] << " datafile.dat" << std::endl;
  }
  return EXIT_FAILURE;
}

// vim: ts=2 sw=2 et
