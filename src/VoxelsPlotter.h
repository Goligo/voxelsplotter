#ifndef VOXELSPLOTTER_H
#define VOXELSPLOTTER_H

#include <vector>
#include <string>
#include <vtkUnsignedCharArray.h>
#include <vtkCharArray.h>
#include <vtkPoints.h>
#include <vtkVoxel.h>
#include <vtkSmartPointer.h>
#include <vtkCellData.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDataSetMapper.h>
#include <vtkActor.h>
#include <vtkRenderWindow.h>
#include <vtkRenderer.h>
#include <vtkRenderWindowInteractor.h>
#include <vtkProperty.h>
#include <vtkXMLUnstructuredGridWriter.h>

const double  color1[] = {221,  75,  75};
const double  color2[] = {230, 230,  55};
const double  color3[] = { 75,  75, 242};

class VoxelsPlotter
{
public:
  VoxelsPlotter();
  void addVoxel(double xMin, double xMax, double yMin, double yMax,
      double zMin, double zMax, int classification);
  void fillUnstructuredGrid();
  void plot();
  void writeFile(std::string fileName);

private:
  vtkSmartPointer<vtkPoints> _points;
  vtkSmartPointer<vtkUnsignedCharArray> _colors;
  vtkSmartPointer<vtkCharArray> _classification;
  vtkSmartPointer<vtkUnstructuredGrid> _voxelsGrid;
  std::vector<vtkSmartPointer<vtkVoxel>> _voxels;
};

#endif // VOXELSPLOTTER_H

// vim: ts=2 sw=2 et
