/*
 * An attempt to draw 3d boxes with VTK
 *
 * Romain Reignier
 * 10/02/2016
 */
#include "VoxelsPlotter.h"

/*
 * Voxel representation
 *    6 +-------------+ 7
 *     /|            /|
 *    / |           / |
 * 4 /  |        5 /  |
 *  +-------------+   |
 *  |  2+---------|---+ 3
 *  |  /          |  /
 *  | /           | /
 *  |/            |/
 *  +-------------+
 * 0               1
 */

VoxelsPlotter::VoxelsPlotter()
{
  _points = vtkSmartPointer<vtkPoints>::New();

  _colors =  vtkSmartPointer<vtkUnsignedCharArray>::New();
  _colors->SetNumberOfComponents(3);
  _colors->SetName("color");

  _classification =  vtkSmartPointer<vtkCharArray>::New();
  _classification->SetNumberOfComponents(1);
  _classification->SetName("classification");

  _voxelsGrid = vtkSmartPointer<vtkUnstructuredGrid>::New();
}

void VoxelsPlotter::addVoxel(double xMin, double xMax,
    double yMin, double yMax, double zMin, double zMax, int classification)
{
  // add new points
  _points->InsertNextPoint(xMin, yMin, zMin);
  _points->InsertNextPoint(xMax, yMin, zMin);
  _points->InsertNextPoint(xMin, yMax, zMin);
  _points->InsertNextPoint(xMax, yMax, zMin);
  _points->InsertNextPoint(xMin, yMin, zMax);
  _points->InsertNextPoint(xMax, yMin, zMax);
  _points->InsertNextPoint(xMin, yMax, zMax);
  _points->InsertNextPoint(xMax, yMax, zMax);

  // add a color
  if (classification == -1) {
      _colors->InsertNextTuple(color1);
  } else if (classification == 0) {
      _colors->InsertNextTuple(color2);
  } else if (classification == 1) {
      _colors->InsertNextTuple(color3);
  }

  _classification->InsertNextTuple1(classification);

  // add a voxel
  _voxels.push_back(vtkSmartPointer<vtkVoxel>::New());
  _voxels.back()->GetPointIds()->SetNumberOfIds(8); 
  for (int j = 0; j < 8; j++) {
    _voxels.back()->GetPointIds()->SetId(j,j + ((_voxels.size() - 1) * 8));
  }
}

void VoxelsPlotter::fillUnstructuredGrid()
{
  if (_voxelsGrid->GetNumberOfCells() == 0) {
    _voxelsGrid->Allocate(_voxels.size());
    _voxelsGrid->SetPoints(_points);
    for (auto& voxel : _voxels) {
        _voxelsGrid->InsertNextCell(voxel->GetCellType(), voxel->GetPointIds());
    }

    // pass the colors and classification to the grid's data cell
    _voxelsGrid->GetCellData()->AddArray(_colors);
    _voxelsGrid->GetCellData()->SetActiveScalars("color");
    _voxelsGrid->GetCellData()->AddArray(_classification);
  }
}

void VoxelsPlotter::plot()
{
  std::cout << "Plotting..." << std::endl;

  // fill the unstructured grid
  fillUnstructuredGrid();

  // create a mapper for the voxels
  vtkSmartPointer<vtkDataSetMapper> voxelsMapper =
    vtkSmartPointer<vtkDataSetMapper>::New();
  voxelsMapper->SetInputData(_voxelsGrid);

  // create an actor
  vtkSmartPointer<vtkActor> voxelsActor =
    vtkSmartPointer<vtkActor>::New();
  voxelsActor->SetMapper(voxelsMapper);
  //voxelsActor->GetProperty()->EdgeVisibilityOn();

  // create the renderer
  vtkSmartPointer<vtkRenderer> renderer =
    vtkSmartPointer<vtkRenderer>::New();

  // create the render window
  vtkSmartPointer<vtkRenderWindow> renderWindow =
    vtkSmartPointer<vtkRenderWindow>::New();
  renderWindow->AddRenderer(renderer);

  // create interactor
  vtkSmartPointer<vtkRenderWindowInteractor> interactor =
    vtkSmartPointer<vtkRenderWindowInteractor>::New();
  interactor->SetRenderWindow(renderWindow);

  renderer->AddActor(voxelsActor);

  // set window background color
  renderer->SetBackground(0.8, 0.8, 0.8);

  std::cout << "Press e or q to quit." << std::endl;

  // render an image (lights and cameras are created automatically)
  renderWindow->Render();

  // begin mouse interaction
  interactor->Start();
}

void VoxelsPlotter::writeFile(std::string fileName)
{
  fileName += ".vtu";
  std::cout << "Writting to the file \"" << fileName << "\"..." << std::endl;
  // fill the unstructured grid if not already
  fillUnstructuredGrid();
  // write the file
  vtkSmartPointer<vtkXMLUnstructuredGridWriter> writer =
    vtkSmartPointer<vtkXMLUnstructuredGridWriter>::New();
  writer->SetFileName(fileName.c_str());
  writer->SetInputData(_voxelsGrid);
  writer->Write();
}

// vim: ts=2 sw=2 et
